// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import {firestorePlugin} from 'vuefire'
import firebase from 'firebase/app'
require('firebase/firestore')
import "chart.js";
import "hchs-vue-charts";
Vue.use(window.VueCharts);

Vue.config.productionTip = false
Vue.use(firestorePlugin)
let firebaseConfig = {
  apiKey: 'AIzaSyCD9Tyu_qXW4-d3fQJ_GaR0zJ0TQhcmUUM',
  authDomain: 'iot-2171e.firebaseapp.com',
  databaseURL: 'https://iot-2171e.firebaseio.com',
  projectId: 'iot-2171e',
  storageBucket: 'iot-2171e.appspot.com',
  messagingSenderId: '692831118060',
  appId: '1:692831118060:web:1ce85d8a36e365f5b43bc9'
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)



export const db = firebase.firestore()

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
