import RPi.GPIO as GPIO
import time
import os
import Adafruit_DHT

humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT11, 4)

humidity = round(humidity, 2)
temperature = round(temperature, 2)

GPIO.setmode(GPIO.BCM)

GPIO.setup(18, GPIO.IN)

while True:
    print(GPIO.input(18))
    print('Temperatuur: ' + str(temperature))
    print('Humidity: ' + str(humidity))
    time.sleep(1)
