import firebase_admin
from firebase_admin import credentials, firestore
from datetime import datetime
import time
import RPi.GPIO as GPIO
import Adafruit_DHT

humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT11, 4)

humidity = round(humidity, 2)
temperature = round(temperature, 2)

GPIO.setmode(GPIO.BCM)

GPIO.setup(18, GPIO.IN)

cred = credentials.Certificate(
    "../iot-2171e-firebase-adminsdk-k0t3r-3efe9e4540.json")
default_app = firebase_admin.initialize_app(cred)
db = firestore.client()

temperature_last = db.collection(u'common').document(u'temperature-last')
humidity_last = db.collection(u'common').document(u'humidity-last')
tilt_last = db.collection(u'common').document(u'tilt-last')

GPIO.setmode(GPIO.BOARD)

#define the pin that goes to the circuit
pin_to_circuit = 7

def rc_time (pin_to_circuit):
    count = 0
  
    #Output on the pin for 
    GPIO.setup(pin_to_circuit, GPIO.OUT)
    GPIO.output(pin_to_circuit, GPIO.LOW)
    time.sleep(0.1)

    #Change the pin back to input
    GPIO.setup(pin_to_circuit, GPIO.IN)
  
    #Count until the pin goes high
    while (GPIO.input(pin_to_circuit) == GPIO.LOW):
        count += 1

    return count


while True:
    temperature_last.set({
        u'value': temperature,
        u'datetime': datetime.now()
    })
    humidity_last.set({
      u'value': humidity,
      u'datetime': datetime.now()
    })
    tilt_last.set({
      u'value': GPIO.input(18),
      u'datetime': datetime.now()
    })

    print(u'Written data.')
    print(u'temp: ' + str(temperature))
    print(u'hum: ' + str(humidity))
    print(u'tilt: ' + str(GPIO.input(18)))
    print(u'light: ' + str(rc_time(pin_to_circuit)))
    time.sleep(5)
